# git branch PS1
export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\] \[\033[33;1m\]\w\[\033[m\] (\$(git branch 2>/dev/null | grep '^*' | colrm 1 2))\n\$ "

# python
alias bp='bpython'
export PYTHONPATH=/usr/bin/python3

# Debian packaging
export DEBEMAIL='sergiosacj@riseup.net'
export DEBFULLNAME='Sergio de Almeida Cipriano Junior'
export USCAN_SYMLINK=rename
alias gbp-sbuild='gbp buildpackage --git-builder=sbuild --git-dist=sid'
alias run-lintian='lintian -c -I --pedantic --color auto ../*dsc ../*deb ../*changes'
alias uscan-check='uscan --verbose --report'
alias check-license="egrep -sriA25 '(public dom|copyright)' | less"
alias sbuild-debug='sbuild --build-failed-commands=%SBUILD_SHELL'
alias run-autopkgtest='autopkgtest -BU -s ../*deb -- schroot chroot:unstable-amd64-sbuild'
alias run-autopkgtest-incus='autopkgtest -BU -s ../*deb -- incus local:debian-sid'
alias git-clean-hard='git reset --hard && git clean -xfd'

# Default editor
export EDITOR=nvim
export VISUAL=nvim

# GPG
alias gpg-list='gpg --list-secret-keys --keyid-format=long'
alias gpg-export='gpg --armor --export 4573E9D2AD27DB896BBF2F9EF15DCE5316051F27'
# send-key: gpg --export sergiosacj@riseup.net | curl -T - https://keys.openpgp.org
alias cafff='caff --keys-from-gnupg -R'
export GPG_TTY=$(tty) # Fix inappropriate ioctl for device

# DevOps
alias show-ports-in-use='sudo lsof -i -P -n | grep LISTEN'

# taskwarrior
alias tt="taskwarrior-tui"

# Git bare for dotfiles
# clone usig --bare
# dotfiles config --local status.showUntrackedFiles no
alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias dotfiles-show-tree='dotfiles ls-tree -r HEAD --name-only'

# Kitty
alias kssh="kitty +kitten ssh"
