# Install Neovim

```
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim
sudo tar -C /opt -xzf nvim-linux64.tar.gz
```

# Fonts

I am using CodeNewRomanNerdFont (https://www.nerdfonts.com), to install it
just put the .otf files in .local/share/fonts.

# Packages needed

- ripgrep (telescope)
- make (telescope)
- npm (for installing mason servers)
