-- JSON Formatting Command for Neovim
vim.api.nvim_create_user_command('JSONFormat', function()
    -- Check if jq is installed
    if vim.fn.executable('jq') == 1 then
        -- Format and sort JSON keys in the entire buffer
        vim.cmd('%!jq --sort-keys .')
    else
        print('Error: jq is not installed. Please install jq to use JSONFormat.')
    end
end, {})

vim.api.nvim_create_autocmd('BufWritePre', {
  pattern = '*.json',
  callback = function()
    vim.cmd('JSONFormat')
  end,
})
