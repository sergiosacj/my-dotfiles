local treesitter_languages = {
  "lua",
  "markdown",
  "markdown_inline",
  "bash",
  "python",
  "c",
  "html",
  "css",
  "javascript",
  "json",
  "go",
  "gomod",
  "gosum",
  "dockerfile",
  "make",
  "regex",
  "ruby",
  "sql",
  "toml",
  "typescript",
}

return treesitter_languages
