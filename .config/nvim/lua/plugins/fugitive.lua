local M = {
  "tpope/vim-fugitive",
  commit = "99db68d9b3304580bd383da7aaee05c7a954a344",
  event = "BufReadPre",
}

return M
