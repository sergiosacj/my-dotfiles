local M = {
  "lukas-reineke/indent-blankline.nvim",
  event = "BufEnter",
}

function M.config()
  require("ibl").setup{}
end

return M
