local M = {
  "ahmedkhalf/project.nvim",
  event = "BufEnter",
}

function M.config()
  require("project_nvim").setup{}
end

return M
