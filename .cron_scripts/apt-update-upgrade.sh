#!/bin/sh

echo "DATE" >> /var/log/sergiosacj.cron.log 2>&1
date >> /var/log/sergiosacj.cron.log 2>&1
DEBIAN_FRONTEND=noninteractive apt-get update -y >> /var/log/sergiosacj.cron.log 2>&1
DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y >> /var/log/sergiosacj.cron.log 2>&1
DEBIAN_FRONTEND=noninteractive apt-get autoremove -y >> /var/log/sergiosacj.cron.log 2>&1
